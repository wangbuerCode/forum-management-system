/**
 * 
 */
package com.web.service;

import com.web.bean.Helps;
import com.web.bean.Pages;

import java.util.List;

/**
 * @author pieryon
 * @email peiyajun2005@163.com
 */
public interface HelpService {
	public List<Helps> getAll();

	/**
	 * @param pageSize
	 * @param nowPage
	 * @return
	 */
	public Pages ManageAllForPages(int pageSize, int nowPage);

	/**
	 * @param index
	 * @return
	 */
	public Helps find(int index);

	/**
	 * @param thelp
	 */
	public void ManageAdd(Helps thelp);

	/**
	 * @param thelp
	 */
	public void ManageUpdate(Helps thelp);
}
