/**
 * 
 */
package com.web.dao;

import com.web.bean.Comments;

/**
 * @author pieryon
 * @email peiyajun2005@163.com
 */
public interface CommentDao {
	public boolean add(Comments comment);

	/**
	 * @param comments
	 */
	public void update(Comments comments);

	/**
	 * @param id
	 * @return
	 */
	public Comments find(int id);

	/**
	 * @param comment
	 */
	public void delete(Comments comment);

}
