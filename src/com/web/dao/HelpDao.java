/**
 * 
 */
package com.web.dao;

import com.web.bean.Helps;

import java.util.List;

/**
 * @author pieryon
 * @email peiyajun2005@163.com
 */
public interface HelpDao {
	public List<Helps> getAll();

	/**
	 * @param index
	 * @return
	 */
	public Helps find(int index);

	/**
	 * @param thelp
	 */
	public void add(Helps thelp);

	/**
	 * @param thelp
	 */
	public void update(Helps thelp);

}
