/**
 * 
 */
package com.web.dao.impl;

import com.web.bean.Grades;
import com.web.dao.GradeDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * @author pieryon
 * @email peiyajun2005@163.com
 */
public class GradeDaoImpl implements GradeDao{
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	@Override
	public Grades find(int id) {
		Session session = sessionFactory.getCurrentSession();
		Grades grade = (Grades) session.get(Grades.class, id);
		return grade;
	}

}
