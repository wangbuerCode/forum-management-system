/**
 * 
 */
package com.web.service;


import com.web.bean.Pages;
import com.web.bean.Types;

/**
 * @author pieryon
 * @email peiyajun2005@163.com
 */

public interface TypeService {
	public Pages getAllForPages(int pageSize, int nowPage, int typeId);

	/**
	 * @param id
	 * @return
	 */
	public Types find(int id);

	/**
	 * @param type
	 */
	public void add(Types type);

	/**
	 * @param type
	 */
	public void update(Types type);
	
}
