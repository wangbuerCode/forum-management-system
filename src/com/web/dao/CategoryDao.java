/**
 * 
 */
package com.web.dao;

import com.web.bean.Categorys;

import java.util.List;

/**
 * @author pieryon
 * @email peiyajun2005@163.com
 */
public interface CategoryDao {
	public boolean add(Categorys cate);
	public boolean delete(int index);
	public boolean update(Categorys cate);
	public List<Categorys> getAll();
	/**
	 * @param id
	 * @return
	 */
	public Categorys find(int id);
}
