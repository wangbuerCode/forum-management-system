/**
 * 
 */
package com.web.service;

import com.web.bean.Comments;
import com.web.bean.Topics;
import com.web.bean.Users;

/**
 * @author pieryon
 * @email peiyajun2005@163.com
 */
public interface CommentService {

	public boolean newComment(Comments comment, Users user, Topics topic);


	/**
	 * @param comment
	 * @param topic 
	 */
	public void deleteComment(Comments comment, Topics topic);
}
