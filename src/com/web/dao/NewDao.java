/**
 * 
 */
package com.web.dao;

import com.web.bean.News;

/**
 * @author pieryon
 * @email peiyajun2005@163.com
 */
public interface NewDao {
	public boolean add(News tnew);

	public boolean update(News tnew);

	/**
	 * @param id
	 * @return
	 */
	public News find(int id);

	/**
	 * @param tnew
	 */
	public void delete(News tnew);
}
