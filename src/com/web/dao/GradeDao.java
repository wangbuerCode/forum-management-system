/**
 * 
 */
package com.web.dao;

import com.web.bean.Grades;

/**
 * @author pieryon
 * @email peiyajun2005@163.com
 */
public interface GradeDao {

	/**
	 * @param id
	 * @return
	 */
	public Grades find(int id);

}
