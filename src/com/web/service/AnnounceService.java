/**
 *
 */
package com.web.service;

import com.web.bean.Announces;
import com.web.bean.Pages;

import java.util.List;

/**
 * @author pieryon
 * @email peiyajun2005@163.com
 */
public interface AnnounceService {
	public List<Announces> getIndexAnno(int index);

	/**
	 * @return
	 */
	public List<Announces> getAll();

	/**
	 * @param pageSize
	 * @param nowPage
	 * @return
	 */
	public Pages ManageAllForPages(int pageSize, int nowPage);

	/**
	 * @param id
	 */
	public Announces find(int id);

	/**
	 * @param announce
	 */
	public void ManageUpdate(Announces announce);

	/**
	 * @param announce
	 */
	public void ManageAdd(Announces announce);
}
