/**
 * 
 */
package com.web.util;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author pieryon
 *
 */
public class ResponseUtil {
	public static void write(HttpServletResponse response,Object obj) throws IOException{
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		out.print(obj.toString());
		out.flush();
		out.close();
	}

}
