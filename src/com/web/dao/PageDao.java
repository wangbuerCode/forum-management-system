/**
 * 
 */
package com.web.dao;

import java.util.List;

/**
 * @author pieryon
 * @email peiyajun2005@163.com
 */
public interface PageDao {
	public int getAllRowCount(String hql);
	public List query_Objects_ForPages(String hql, int offset, int length);
}
